# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'German DATEV Template Collection for SKR03 GmbH 2012 Timeline',
    'name_de_DE': 'Deutsche DATEV Vorlagensammlung für SKR03 GmbH 2012 Gültigkeitsdauer',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides DATEV Template Collection for SKR03 GmbH Timeline
      for the year 2012
    ''',
    'description_de_DE': '''
    - DATEV Vorlagen Sammlung für SKR03 GmbH mit Gültigkeitsdauer
      für das Jahr 2012
    ''',
    'depends': [
        'account_de_skr03_gmbh_2012_timeline',
        'account_timeline_tax_de_datev',
    ],
    'xml': [
        'account_de_skr03_gmbh_2012_timeline_datev.xml'
    ],
    'translation': [
        # 'de_DE.csv',
    ],
}
